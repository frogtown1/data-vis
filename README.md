# data-vis

Directory of various data visualizations I've worked on. Intended more as a boilerplate for more complicated works.
The demos for each are available:
*  [spiral bar comparison](https://mcgriff-amcharts4-spiral-bar.netlify.com/)
*  [stacked bar chart](https://mcgriff-amcharts4-stacked-bar.netlify.com/)
*  [tri-relational chart](https://mcgriff-amcharts4-tri-rel.netlify.com/)