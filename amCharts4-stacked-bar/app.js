"use strict";

// Call themes
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_dataviz);

// Create chart instance
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const chart = am4core.create("chartdiv", am4charts.XYChart);


// Add data; formatted in an array
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
chart.data = [{
  "year": "16-24",
  "regular": 19,
  "special": 33,
  "occasional": 25,
  "rarely": 19,
  "never": 14,
}, {
  "year": "25-34",
  "regular": 34,
  "special": 23,
  "occasional": 22,
  "rarely": 18,
  "never": 16,
}, {
  "year": "35-44",
  "regular": 33,
  "special": 15,
  "occasional": 15,
  "rarely": 15,
  "never": 15,
}];

// Create axes
const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "year";
categoryAxis.renderer.grid.template.location = 0;


const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.disabled = true;
valueAxis.min = 0;

// Create series
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function createSeries(field, name) {

  // Set up series
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  const series = chart.series.push(new am4charts.ColumnSeries());
  series.name = name;
  series.dataFields.valueY = field;
  series.dataFields.categoryX = "year";
  series.sequencedInterpolation = true;

  // Make it stacked
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  series.stacked = true;

  // Configure columns
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  series.columns.template.width = am4core.percent(60);
  series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

  // Add labels
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  const labelBullet = series.bullets.push(new am4charts.LabelBullet());
  labelBullet.label.text = "{valueY}";
  labelBullet.label.hideOversized = true;
  labelBullet.label.fill = am4core.color("#fff");
  labelBullet.locationY = 0.5;

  return series;
}

createSeries("regular", "Regular");
createSeries("special", "Special");
createSeries("occasional", "Occasional");
createSeries("rarely", "Rarely");
createSeries("never", "Never");

// Legend
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
chart.legend = new am4charts.Legend();

// Modify PDF export
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
chart.exporting.menu = new am4core.ExportMenu();

// Do not add URL
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
chart.exporting.getFormatOptions("pdf").addURL = false;


chart.exporting.adapter.add("pdfmakeDocument", function(pdf, target) {

  // Add title to the beginning
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  pdf.doc.content.unshift({
    text: "Age Breakdown of the Luxury Buyer",
    margin: [0, 30],
    style: {
      fontSize: 25,
      bold: true,
    }
  });

  
  // Add a two-column intro
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  pdf.doc.content.push({
    alignment: 'justify',
    columns: [
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.'
      },
      {
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Malit profecta versatur nomine ocurreret multavit, officiis viveremus aeternum superstitio suspicor alia nostram, quando nostros congressus susceperant concederetur leguntur iam, vigiliae democritea tantopere causae, atilii plerumque ipsas potitur pertineant multis rem quaeri pro, legendum didicisse credere ex maluisset per videtis. Cur discordans praetereat aliae ruinae dirigentur orestem eodem, praetermittenda divinum. Collegisti, deteriora malint loquuntur officii cotidie finitas referri doleamus ambigua acute. Adhaesiones ratione beate arbitraretur detractis perdiscere, constituant hostis polyaeno. Diu concederetur.'
      }
    ],
    columnGap: 20,
    margin: [0, 30]
  });

  return pdf;
});