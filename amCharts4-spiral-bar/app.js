"use strict";

// Themes begin
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
am4core.useTheme(am4themes_animated);
// Themes end

const chart = am4core.create("chartdiv", am4plugins_timeline.SpiralChart);
chart.levelCount = 3;
chart.inversed = true;
chart.endAngle = -135;
chart.yAxisInnerRadius = 0;
chart.yAxisRadius = am4core.percent(70);
chart.padding(0,0,0,0)

chart.data = [
  { category: "" },
  { category: "Federer (SUI)", value: 20 },
  { category: "Nadal (SPA)", value: 19 },
  { category: "Djokovic (SRB)", value: 16 },
  { category: "Sampras (USA)", value: 14 },
  { category: "Emerson (AUS)", value: 12 },
  { category: "Borg (SWE)", value: 11 }];

chart.fontSize = 11;

const categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "category";
categoryAxis.renderer.grid.template.disabled = true;
categoryAxis.renderer.minGridDistance = 6;
categoryAxis.cursorTooltipEnabled = false;


const categoryAxisLabelTemplate = categoryAxis.renderer.labels.template;
categoryAxisLabelTemplate.paddingLeft = 20;
categoryAxisLabelTemplate.horizontalCenter = "left";
categoryAxisLabelTemplate.adapter.add("rotation", function (rotation, target) {
  const position = valueAxis.valueToPosition(valueAxis.min);
  return valueAxis.renderer.positionToAngle(position) + 90;
})


const valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.minGridDistance = 70;

valueAxis.renderer.line.strokeDasharray = "1,4";
valueAxis.renderer.line.strokeOpacity = 0.7;
valueAxis.renderer.grid.template.disabled = true;
valueAxis.zIndex = 100;
valueAxis.cursorTooltipEnabled = false;
valueAxis.min = 0;

const labelTemplate = valueAxis.renderer.labels.template;
labelTemplate.verticalCenter = "middle";
labelTemplate.fillOpacity = 0.7;

const series = chart.series.push(new am4plugins_timeline.CurveColumnSeries());
series.dataFields.valueX = "value";
series.dataFields.categoryY = "category";

const columnTemplate = series.columns.template;
series.tooltipText = "{categoryY}: {valueX} grand slams";
columnTemplate.adapter.add("fill", function (fill, target) {
  return chart.colors.getIndex(target.dataItem.index * 2);
})
columnTemplate.strokeOpacity = 0;
columnTemplate.fillOpacity = 0.8;

const hoverState = columnTemplate.states.create("hover")
hoverState.properties.fillOpacity = 1;


chart.scrollbarX = new am4core.Scrollbar();
chart.scrollbarX.align = "center"
chart.scrollbarX.width = am4core.percent(70);
chart.isMeasured = false;

const cursor = new am4plugins_timeline.CurveCursor();
chart.cursor = cursor;
cursor.xAxis = valueAxis;
cursor.yAxis = categoryAxis;
cursor.lineY.disabled = true;
cursor.lineX.strokeDasharray = "1,4";
cursor.lineX.strokeOpacity = 1;

const label = chart.plotContainer.createChild(am4core.Label);
label.text = "ATP Grand Slam Victories"
label.fontSize = 18;
label.x = am4core.percent(80);
label.y = am4core.percent(80);
label.horizontalCenter = "right";