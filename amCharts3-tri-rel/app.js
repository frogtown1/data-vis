"use strict";
const chartData = [
    {
        "date": "2019-10-18",
        "volume": 6.53,
        // "townName": "MC: 25.02",
        "townName2": "MC: 25.02",
        "townSize": 25,
        "market cap": 25.02,
        "price": 0.0413
    },
    {
        "date": "2019-10-19",
        "volume": 4.08,

        "townSize": 24,
        "market cap": 23.93,
        "price": 0.0395
    },
    {
        "date": "2019-10-20",
        "volume": 7.69,

        "townSize": 24,
        "market cap": 24.11,
        "price": 0.0398
    },
    {
        "date": "2019-10-21",
        "volume": 7.55,
        
        "townSize": 24,
        "market cap": 23.96,
        "price": 0.0396
    },
    {
        "date": "2019-10-22",
        "volume": 7.10,
        "townName": "MC: 24.56",
        "townName2": "MC: 24.56",
        "townSize": 24,
        "market cap": 24.56,
        "price": 0.0406
    },
    {
        "date": "2019-10-23",
        "volume": 5.84,
    
        "townSize": 24,
        "market cap": 24.15,
        "price": 0.0399
    },
    {
        "date": "2019-10-24",
        "volume": 7.16,
    
        "townSize": 22,
        "market cap": 21.74,
        "price": 0.0359
    },
    {
        "date": "2019-10-25",
        "volume": 5.98,
        "townSize": 22,
        "market cap": 22.49,
        "price": 0.0371
    },
    {
        "date": "2019-10-26",
    },
    {
        "date": "2019-10-27",
    }
    ,
    {
        "date": "2019-10-28",
    }
    ,
    {
        "date": "2019-10-29",
    }
    ,
    {
        "date": "2019-10-30",
    },
    {
        "date": "2019-10-31",
    }
];
const chart = AmCharts.makeChart("chartdiv", {
  type: "serial",
  theme: "dark",
  dataDateFormat: "YYYY-MM-DD",
  dataProvider: chartData,

  addClassNames: true,
  startDuration: 1,
  color: "#FFFFFF",
  marginLeft: 0,

  categoryField: "date",
  categoryAxis: {
    parseDates: true,
    minPeriod: "DD",
    autoGridCount: false,
    gridCount: 50,
    gridAlpha: 0.1,
    gridColor: "#FFFFFF",
    axisColor: "#555555",
    dateFormats: [{
        period: 'DD',
        format: 'DD'
    }, {
        period: 'WW',
        format: 'MMM DD'
    }, {
        period: 'MM',
        format: 'MMM'
    }, {
        period: 'YYYY',
        format: 'YYYY'
    }]
  },

  valueAxes: [{
    id: "a1",
    title: "volume",
    gridAlpha: 0,
    axisAlpha: 0
  },{
    id: "a2",
    position: "right",
    gridAlpha: 0,
    axisAlpha: 0,
    labelsEnabled: false
  },{
    id: "a3",
    title: "price (USD)",
    position: "right",
    gridAlpha: 0,
    axisAlpha: 0,
    inside: true,
    price: "$",
    priceUnits: {
        DD: "USD "
    }
  }],
  graphs: [{
    id: "g1",
    valueField:  "volume",
    title:  "volume (millions USD)",
    type:  "column",
    fillAlphas:  0.8,
    valueAxis:  "a1",
    balloonText:  "[[value]]",
    legendValueText:  "[[value]]",
    legendPeriodValueText:  "total: [[value.sum]] USD",
    lineColor:  "darkgray",
    alphaField:  "alpha",
  },{
    id: "g2",
    valueField: "market cap",
    classNameField: "bulletClass",
    title: "market cap (millions USD)",
    type: "line",
    valueAxis: "a2",
    lineColor: "#54FFDD",
    lineThickness: 1,
    legendValueText: "[[description]]/[[value]]",
    descriptionField: "townName",
    bullet: "round",
    bulletSizeField: "townSize",
    bulletBorderColor: "#54FFDD",
    bulletBorderAlpha: 1,
    bulletBorderThickness: 2,
    bulletColor: "#000000",
    labelText: "[[townName2]]",
    labelPosition: "right",
    balloonText: "market cap:[[value]]",
    showBalloon: true,
    animationPlayed: true,
  },{
    id: "g3",
    title: "price (USD)",
    valueField: "price",
    type: "line",
    valueAxis: "a3",
    lineColor: "#CC2F3D",
    balloonText: "[[value]]",
    lineThickness: 1,
    legendValueText: "[[value]]",
    bullet: "square",
    bulletBorderColor: "#CC2F3D",
    bulletBorderThickness: 1,
    bulletBorderAlpha: 1,
    dashLengthField: "dashLength",
    animationPlayed: true
  }],

  chartCursor: {
    zoomable: false,
    categoryBalloonDateFormat: "DD",
    cursorAlpha: 0,
    valueBalloonsEnabled: false
  },
  legend: {
    bulletType: "round",
    equalWidths: false,
    valueWidth: 120,
    useGraphSettings: true,
    color: "#FFFFFF"
  }
});